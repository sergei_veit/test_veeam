## Run the app

0. ```npm install```
0. ```npm start```
0. Open in browser [http://localhost:3200/](http://localhost:3200/)

## Build the app
```npm run build```

## Test application

#####With watcher
```npm test```

#####Without watcher:
```npm test --a```

