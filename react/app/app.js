import React from 'react';
import Routes from './routes';
import './styles/app.scss';
import 'react-table/react-table.css';

const App = () => {
    return (
        <div className="application">
            <main className="main">
                {Routes}
            </main>
        </div>
    );
};

export default App;
