import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Checkbox = (props) => {
    const attr = {
        value: isTypes(props.value, 'string') ? props.value : 0
    };

    return (
        <input
            name={props.name}
            id={props.name}
            type="checkbox"
            checked={![undefined, false, 'false', 0, '0', null, 'none', 'no'].includes(props.checked)}
            {...attr}
        />
    );
};

Checkbox.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.string,
    checked: React.PropTypes.any
};

export default Checkbox;
