import React from 'react';
import Presentation from './presentation';

class ConfigContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // eslint-disable-next-line no-undef
            value: window.JSON5.stringify(props.value)
        };
    }

    handleChangeText(e) {
        this.setState({
            value: e.target.value
        });
    }

    handleApply() {
        let json = null;
        try {
            // eslint-disable-next-line no-undef
            json = window.JSON5.parse(this.state.value);
        } catch (e) {
            window.alert('It is not a json.');
            return;
        }
        this.props.onApply(json);
    }

    render() {
        return (
            <Presentation
                value={this.state.value}
                onChange={this.handleChangeText.bind(this)}
                onApply={this.handleApply.bind(this)}
            />
        );
    }
}

ConfigContainer.propTypes = {
    onApply: React.PropTypes.func,
    value: React.PropTypes.object
};

export default ConfigContainer;
