import React from 'react';

const Presentation = (props) => {
    return (
        <div className="config active">
            <textarea
                value={props.value}
                onChange={props.onChange}
            />
            <button
                className="apply"
                onClick={props.onApply}
            >
                Apply
            </button>
        </div>
    );
};

Presentation.propTypes = {
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
    onApply: React.PropTypes.func
};

export default Presentation;
