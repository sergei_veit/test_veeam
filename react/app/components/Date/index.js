import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Date = (props) => {
    const attr = {
        value: isTypes(props.value, 'string') ? props.value : 0
    };
    if (isTypes(props.max, 'string')) {
        attr.max = props.max;
    }

    if (isTypes(props.min, 'string')) {
        attr.min = props.min;
    }

    return (
        <input
            name={props.name}
            id={props.name}
            type="number"
            {...attr}
        />
    );
};

Date.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.string,
    max: React.PropTypes.string,
    min: React.PropTypes.string
};

export default Date;
