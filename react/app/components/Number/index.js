import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Number = (props) => {
    const attr = {
        value: isTypes(props.value, 'string', 'number') && !isNaN(props.value) ? props.value : 0
    };
    if (isTypes(props.max, 'number', 'string') && !isNaN(props.max)) {
        attr.max = props.max;
    }

    if (isTypes(props.min, 'number', 'string') && !isNaN(props.min)) {
        attr.min = props.min;
    }

    return (
        <input
            name={props.name}
            id={props.name}
            type="number"
            {...attr}
        />
    );
};

Number.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.any,
    max: React.PropTypes.any,
    min: React.PropTypes.any
};

export default Number;
