import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Radio = (props) => {
    const attr = {
        value: isTypes(props.value, 'string') ? props.value : 0
    };

    return (
        <label>
            <input
                name={props.name}
                type="radio"
                checked={![undefined, false, 'false', 0, '0', null, 'none', 'no'].includes(props.checked)}
                {...attr}
            />
            {props.label && (
                <span>
                    {props.label}
                </span>
            )}
        </label>
    );
};

Radio.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.string,
    label: React.PropTypes.string,
    checked: React.PropTypes.any
};

export default Radio;
