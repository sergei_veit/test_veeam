import React from 'react';
import Presentation from './presentation';
import { isTypes } from '../../helpers/TypesHelper';

class ResultContainer extends React.Component {
    getTitle(title) {
        if (!isTypes(title, 'string')) {
            return '';
        }

        return title;
    }

    render() {
        return (
            <Presentation
                title={this.getTitle(this.props.title)}
                buttons={this.props.buttons || []}
                items={this.props.items || []}
            />
        );
    }
}

ResultContainer.propTypes = {
    title: React.PropTypes.any,
    buttons: React.PropTypes.any,
    items: React.PropTypes.any
};

export default ResultContainer;
