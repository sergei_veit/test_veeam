import React from 'react';
import { isTypes } from '../../../helpers/TypesHelper';

const Buttons = (props) => {
    return (
        <div className="buttons">
            {props.buttons.map((button, index) => {
                if (!isTypes(button, 'string')) {
                    return null;
                }

                return (
                    <button key={index}>{button}</button>
                );
            })}
        </div>
    );
};

Buttons.propTypes = {
    buttons: React.PropTypes.array
};

export default Buttons;
