import React from 'react';
import { isTypes } from '../../../helpers/TypesHelper';
import Number from '../../Number';
import Radio from '../../Radio';
import WrapItem from '../../WrapItem';
import Textarea from '../../Textarea';
import Checkbox from '../../Checkbox';
import Text from '../../Text';
import Date from '../../Date';

const Content = (props) => {
    return (
        <div className="result-wrapper">
            {props.items.map((item, index) => {
                const labelString = isTypes(item.label, 'string') ? item.label : '';
                let name = 'name' + index;
                let result = null;

                switch (item.type) {
                    case 'numberfield':
                    case 'number': {
                        result = (
                            <Number
                                {...item}
                                name={name}
                            />
                        );
                        break;
                    }
                    case 'textfield':
                    case 'text': {
                        result = (
                            <Text
                                {...item}
                                name={name}
                            />
                        );
                        break;
                    }
                    case 'textarea': {
                        result = (
                            <Textarea
                                {...item}
                                name={name}
                            />
                        );
                        break;
                    }
                    case 'checkbox': {
                        result = (
                            <Checkbox
                                {...item}
                                name={name}
                            />
                        );
                        break;
                    }
                    case 'datefield':
                    case 'date': {
                        result = (
                            <Date
                                {...item}
                                name={name}
                            />
                        );
                        break;
                    }
                    case 'radio buttons':
                    case 'radio':
                    case 'radiobuttons': {
                        const list = item.items || item.list;
                        if (!list || !Array.isArray(list)) {
                            return null;
                        }
                        result = list.map((element, indexElement) => {
                            return (
                                <Radio
                                    {...element}
                                    key={indexElement}
                                    name={name}
                                />
                            );
                        });
                        name = null;
                        break;
                    }
                    default: {
                        return null;
                    }
                }

                return (
                    <WrapItem
                        label={labelString}
                        name={name}
                        key={index}
                    >
                        {result}
                    </WrapItem>
                );
            })}
        </div>
    );
};

Content.propTypes = {
    items: React.PropTypes.any
};

export default Content;
