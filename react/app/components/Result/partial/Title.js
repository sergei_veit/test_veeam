import React from 'react';

const Title = (props) => {
    return (
        <div className="title">
            {props.title}
        </div>
    );
};

Title.propTypes = {
    title: React.PropTypes.string
};

export default Title;
