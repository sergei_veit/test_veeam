import React from 'react';
import Title from '../partial/Title';
import Buttons from '../partial/Buttons';
import Content from '../partial/Content';

const Presentation = (props) => {
    return (
        <div className="result">
            <Title title={props.title} />
            <hr />
            <Content
                items={props.items}
            />
            <hr />
            <Buttons buttons={props.buttons} />
        </div>
    );
};

Presentation.propTypes = {
    title: React.PropTypes.string,
    items: React.PropTypes.any,
    buttons: React.PropTypes.array
};

export default Presentation;
