import React from 'react';
import classNames from 'classnames';

const Presentation = (props) => {
    return (
        <div className="tabs-wrapper">
            <div className="tabs">
                {props.tabs.map((tab, index) => {
                    const tabClassname = classNames('tabs', { active: index === props.active });
                    return (
                        <button
                            key={index}
                            className={tabClassname}
                            onClick={() => {
                                props.onChangeTab(index);
                            }}
                        >
                            {tab}
                        </button>
                    );
                })}
            </div>
            <div className="tabs-content">
                {props.content.filter((content, index) => {
                    return index === props.active;
                })[0]}
            </div>
        </div>
    );
};

Presentation.propTypes = {
    tabs: React.PropTypes.array,
    active: React.PropTypes.number,
    content: React.PropTypes.any,
    onChangeTab: React.PropTypes.func,
};

export default Presentation;
