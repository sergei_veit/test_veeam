import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Text = (props) => {
    const attr = {
        value: isTypes(props.value, 'string') ? props.value : ''
    };
    if (isTypes(props.maxlength, 'number', 'string') && !isNaN(props.maxlength)) {
        attr.maxlength = props.maxlength;
    }

    return (
        <input
            name={props.name}
            id={props.name}
            type="text"
            {...attr}
        />
    );
};

Text.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.any,
    maxlength: React.PropTypes.any
};

export default Text;
