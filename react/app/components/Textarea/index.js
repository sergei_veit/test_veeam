import React from 'react';
import { isTypes } from '../../helpers/TypesHelper';

const Textarea = (props) => {
    const attr = {
        value: isTypes(props.value, 'string') ? props.value : 0
    };
    if (isTypes(props.maxlength, 'number', 'string') && !isNaN(props.maxlength)) {
        attr.maxlength = props.maxlength;
    }

    return (
        <textarea
            name={props.name}
            id={props.name}
            {...attr}
        />
    );
};

Textarea.propTypes = {
    name: React.PropTypes.string,
    value: React.PropTypes.string,
    maxlength: React.PropTypes.any
};

export default Textarea;
