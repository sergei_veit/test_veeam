import React from 'react';

const WrapItem = (props) => {
    return (
        <div className="element-wrapper">
            <div className="labelFor">
                {props.label && (
                    <label
                        htmlFor={props.name}
                    >
                        {props.label}
                    </label>
                )}
            </div>
            <div className="element">
                {props.children}
            </div>
        </div>
    );
};

WrapItem.propTypes = {
    label: React.PropTypes.any,
    name: React.PropTypes.any,
    children: React.PropTypes.any
};

export default WrapItem;
