import moment from 'moment';
import { isNumber } from './TypesHelper';

const ASC = 'ASC';
const DESC = 'DESC';

const sortDirections = {
    ASC,
    DESC
};

function filterBy(array, path, ident) {
    return array.filter((item) => {
        return (item[path] && item[path] === ident);
    });
}

function includesFilterBy(array, path, ident) {
    return array.filter((item) => {
        return (item[path] && item[path].includes(ident));
    });
}

function findBy(array, path, ident) {
    const result = filterBy(array, path, ident);
    return result.length ? result[0] : null;
}

function sortBy(array, path, direction = ASC) {
    const result = array.sort((a, b) => {
        const aValue = a[path];
        const bValue = b[path];

        if (isNumber(aValue) && isNumber(bValue)) {
            return Number(aValue) - Number(bValue);
        }

        if (moment(aValue).isValid() && moment(bValue).isValid()) {
            return moment(aValue).isAfter(moment(bValue)) ? 1 : -1;
        }

        return aValue > bValue ? 1 : -1;
    });

    return (direction === ASC) ? result : result.reverse();
}

export {
    filterBy,
    findBy,
    sortBy,
    sortDirections,
    includesFilterBy
};
