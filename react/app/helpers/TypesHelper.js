function isTypes(item, ...args) {
    if (args.length > 0) {
        return args.includes(typeof item);
    }

    return false;
}

function isNumber(value) {
    return (
        value !== null
        && isTypes(value, 'number', 'string')
        && Number(value).toString() === value.toString()
        && !isNaN(Number(value))
        && isFinite(Number(value))
    );
}

export {
    isNumber,
    isTypes
};
