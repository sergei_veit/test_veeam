import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import { applyMiddleware, createStore, compose } from 'redux';
import rootReducer from './reducers';
import { Route } from 'react-router-dom';
import App from './app';


const isProd = process.env.NODE_ENV === 'production';

const history = createHistory();
const middleware = routerMiddleware(history);

const appliedMiddleware = isProd
    ? applyMiddleware(middleware)
    : compose(applyMiddleware(middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const store = createStore(
    rootReducer,
    appliedMiddleware
);

render(
    <AppContainer>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Route path="/" component={App} />
            </ConnectedRouter>
        </Provider>
    </AppContainer>,
    document.getElementById('root')
);
