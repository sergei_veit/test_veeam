import React from 'react';
import Tabs from '../../components/Tabs';
import Config from '../../components/Config';
import Result from '../../components/Result';
import { isNumber } from '../../helpers/TypesHelper';

class ContainerIndex extends React.Component {
    state = {
        content: {},
        activeTab: 0
    };

    onApply(content) {
        this.setState({
            content,
            activeTab: 1
        });
    }

    switchTab(index) {
        if (isNumber(index)) {
            this.setState({ activeTab: index });
        }
    }

    render() {
        return (
            <div className="index">
                <Tabs
                    tabs={['Config', 'Result']}
                    active={this.state.activeTab}
                    onChangeTab={this.switchTab.bind(this)}
                    content={[
                        (<Config
                            value={this.state.content}
                            onApply={this.onApply.bind(this)}
                        />),
                        (<Result
                            {...this.state.content}
                        />)
                    ]}
                />
            </div>
        );
    }
}

export default ContainerIndex;
