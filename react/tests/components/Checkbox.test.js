import React from 'react';
import { shallow } from 'enzyme';
import Checkbox from '../../app/components/Checkbox';

describe('Test component Checkbox', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Checkbox />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('checkbox');
    });
});
