import React from 'react';
import { shallow } from 'enzyme';
import Date from '../../app/components/Date';

describe('Test component Date', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Date />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('date');
    });
});
