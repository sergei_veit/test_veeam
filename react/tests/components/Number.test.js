import React from 'react';
import { shallow } from 'enzyme';
import Number from '../../app/components/Number';

describe('Test component Number', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Number />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('number');
    });
});
