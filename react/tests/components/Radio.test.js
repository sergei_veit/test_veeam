import React from 'react';
import { shallow } from 'enzyme';
import Radio from '../../app/components/Radio';

describe('Test component Radio', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Radio />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('radio');
        expect(wrapper.find('span')).toHaveLength(0);
        expect(wrapper.find('label')).toHaveLength(1);
    });
    it('It render with label', () => {
        const wrapper = shallow(
            <Radio label="label" />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('radio');
        expect(wrapper.find('span')).toHaveLength(1);
        expect(wrapper.find('label')).toHaveLength(1);
    });
});
