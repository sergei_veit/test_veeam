import React from 'react';
import { shallow } from 'enzyme';
import Text from '../../app/components/Text';

describe('Test component Text', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Text />
        );
        expect(wrapper.find('input')).toHaveLength(1);
        expect(wrapper.find('input').prop('type')).toBe('text');
    });
});
