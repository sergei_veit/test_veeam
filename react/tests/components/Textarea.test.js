import React from 'react';
import { shallow } from 'enzyme';
import Textarea from '../../app/components/Textarea';

describe('Test component Textarea', () => {
    it('It render', () => {
        const wrapper = shallow(
            <Textarea />
        );
        expect(wrapper.find('textarea')).toHaveLength(1);
    });
});
