import React from 'react';
import { shallow } from 'enzyme';
import WrapItem from '../../app/components/WrapItem';

describe('Test component WrapItem', () => {
    it('It render with label', () => {
        const wrapper = shallow(
            <WrapItem label="label">
                <div className="content" />
            </WrapItem>
        );
        expect(wrapper.find('.element-wrapper')).toHaveLength(1);
        expect(wrapper.find('label')).toHaveLength(1);
        expect(wrapper.find('.content')).toHaveLength(1);
    });
    it('It render without label', () => {
        const wrapper = shallow(
            <WrapItem>
                <div className="content" />
            </WrapItem>
        );
        expect(wrapper.find('.element-wrapper')).toHaveLength(1);
        expect(wrapper.find('label')).toHaveLength(0);
        expect(wrapper.find('.content')).toHaveLength(1);
    });
});
