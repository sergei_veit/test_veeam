import {
    filterBy,
    findBy,
    sortBy,
    sortDirections,
    includesFilterBy

} from '../../app/helpers/ArrayHelpers';

describe('Test example', () => {
    it('filterBy', () => {
        const props = [{ data: true }, { data: true }, { data: false }, { data: 'other' }];
        const result = filterBy(props, 'data', true);
        expect(result).toHaveLength(2);
    });
    it('findBy', () => {
        const props = [{ data: true }, { data: true }, { data: false }, { data: 'other' }];
        const result = findBy(props, 'data', 'other');
        expect(result.data).toBe('other');

        const result2 = findBy(props, 'data', 'something');
        expect(result2).toBe(null);
    });
    it('includesFilterBy', () => {
        const props = [{ data: 'additional' }, { data: 'more' }, { data: 'better' }, { data: 'other' }];
        const result = includesFilterBy(props, 'data', 'er');
        expect(result).toHaveLength(2);
    });
    it('sortBy', () => {
        const props = [{ data: 1 }, { data: 3 }, { data: 2 }, { data: 0 }];
        const result = sortBy(props, 'data', sortDirections.ASC);
        expect(result[0].data).toBe(0);
        expect(result[1].data).toBe(1);
        expect(result[2].data).toBe(2);
        expect(result[3].data).toBe(3);

        const result2 = sortBy(props, 'data', sortDirections.DESC);
        expect(result2[0].data).toBe(3);
        expect(result2[1].data).toBe(2);
        expect(result2[2].data).toBe(1);
        expect(result2[3].data).toBe(0);

        const props2 = [{ data: 'b' }, { data: 'd' }, { data: 'c' }, { data: 'a' }];
        const result3 = sortBy(props2, 'data');
        expect(result3[0].data).toBe('a');
        expect(result3[1].data).toBe('b');
        expect(result3[2].data).toBe('c');
        expect(result3[3].data).toBe('d');
    });
});
