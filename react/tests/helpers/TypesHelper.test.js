import { isNumber, isTypes } from '../../app/helpers/TypesHelper';

describe('TypesHelper test', () => {
    it('isNumber', () => {
        expect(isNumber(null)).toEqual(false);
        expect(isNumber(NaN)).toEqual(false);
        expect(isNumber(Infinity)).toEqual(false);
        expect(isNumber(-Infinity)).toEqual(false);
        expect(isNumber(true)).toEqual(false);
        expect(isNumber('test123')).toEqual(false);
        expect(isNumber('123')).toEqual(true);
        expect(isNumber(123)).toEqual(true);
        expect(isNumber(false)).toEqual(false);
        expect(isNumber(0)).toEqual(true);
    });
    it('isTypes', () => {
        expect(isTypes(null)).toEqual(false);
        expect(isTypes(null, 'string')).toEqual(false);
        expect(isTypes(null, 'object')).toEqual(true);
        expect(isTypes('123', 'number')).toEqual(false);
        expect(isTypes('123', 'number', 'string')).toEqual(true);
    });
});
