import React from 'react';
import { shallow } from 'enzyme';
import Index from '../../app/pages/Index/';

let wrapper = null;

describe('Test page Index', () => {
    beforeEach(() => {
        wrapper = shallow(<Index />);
    });
    it('It render', () => {
        expect(wrapper.find('.index')).toHaveLength(1);
    });
    it('onApply', () => {
        wrapper.setState({
            activeTab: 0,
            content: null
        });
        wrapper.instance().onApply('test');

        expect(wrapper.state('content')).toBe('test');
        expect(wrapper.state('activeTab')).toBe(1);
    });
    it('switchTab', () => {
        wrapper.setState({
            activeTab: 0
        });
        wrapper.instance().switchTab(1);
        expect(wrapper.state('activeTab')).toBe(1);

        wrapper.instance().switchTab(null);
        expect(wrapper.state('activeTab')).toBe(1);

        wrapper.instance().switchTab('test');
        expect(wrapper.state('activeTab')).toBe(1);
    });
});
