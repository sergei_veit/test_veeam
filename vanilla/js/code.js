function convertButtons(buttons) {
    if (!Array.isArray(buttons)) {
        throw new Error("Buttons list is not array of names.");
    }

    return buttons.map(function (button, index) {
        if (!isType(button, 'string')) {
            return throwError("Button " + index + " is not a name of button.")

        }
        var item = document.createElement('button');
        item.innerText = button;

        return item;
    }).filter(function (item) {
        return item;
    });
}

function createItem(tagName, attributes) {
    var item = document.createElement(tagName);
    Object.keys(attributes).forEach(function (key) {
        var value = attributes[key].toString().replace(/"/mg, '').replace(/\n/mg, '');
        item.setAttribute(key, value);
    });

    return item;
}

function wrapItem(item, label, name) {
    var container = createItem('div', {"class": 'element-wrapper'});
    var labelWrapper = createItem('div', {"class": "labelFor"});
    var itemWrapper = createItem('div', {"class": "element"});

    container.className = 'element-wrapper';
    labelWrapper.className = 'labelFor';
    itemWrapper.className = 'element';

    container.append(labelWrapper, itemWrapper);
    if (Array.isArray(item)) {
        item.forEach(function (element) {
            itemWrapper.append(element);
        });
    } else {
        itemWrapper.append(item);
    }

    if (label) {
        var labelItem = createItem("label", {"for": name});
        labelItem.innerText = label;
        labelWrapper.append(labelItem);
    }

    return container;
}

function convertNumberField(props, name) {
    var attr = {
        type: "number",
        name: name,
        id: name,
        value: isType(props.value, "string", "number") && !isNaN(props.value) ? props.value : 0
    };
    var label = isType(props.label, "string") ? props.label : "";

    if (isType(props.max, "number", "string") && !isNaN(props.max)) {
        attr['max'] = props.max;
    }

    if (isType(props.min, "number", "string") && !isNaN(props.min)) {
        attr['min'] = props.min;
    }


    var item = createItem('input', attr);

    return wrapItem(item, label, name);
}

function convertTextField(props, name) {
    var attr = {
        type: "text",
        name: name,
        id: name,
        value: isType(props.value, "string", "number") ? props.value : ""
    };
    var label = isType(props.label, "string") ? props.label : "";

    if (isType(props.maxlength, "number", "string") && !isNaN(props.maxlength)) {
        attr['maxlength'] = props.maxlength;
    }

    var item = createItem('input', attr);

    return wrapItem(item, label, name);
}

function convertDateField(props, name) {
    //here should be moment.js validation
    var attr = {
        type: "date",
        name: name,
        id: name,
        value: isType(props.value, "string") ? props.value : ""
    };
    var label = isType(props.label, "string") ? props.label : "";

    if (isType(props.max, "string")) {
        attr['max'] = props.max;
    }

    if (isType(props.min, "string")) {
        attr['min'] = props.min;
    }


    var item = createItem('input', attr);

    return wrapItem(item, label, name);
}

function convertTextareaField(props, name) {
    var attr = {
        name: name,
        id: name,
        value: isType(props.value, "string", "number") ? props.value : ""
    };
    var label = isType(props.label, "string") ? props.label : "";

    if (isType(props.maxlength, "number", "string") && !isNaN(props.maxlength)) {
        attr['maxlength'] = props.maxlength;
    }

    var item = createItem('textarea', attr);

    return wrapItem(item, label, name);
}

function createSwitcher(props, name) {
    var attr = {
        type: "radio",
        name: name,
        value: isType(props.value, "string", "number") ? props.value : ""
    };
    var label = isType(props.label, "string") ? props.label : "";

    if ([undefined, false, 'false', 0, '0', null].indexOf(props.checked) === -1) {
        attr['checked'] = "checked";
    }

    var item = createItem('input', attr);
    var labelForItem = createItem('label', {"class": "label-wrapper"});
    var textForLabel = createItem('span', {});
    textForLabel.innerText = label;
    labelForItem.append(item);
    labelForItem.append(textForLabel);

    return labelForItem;
}

function convertCheckboxField(props, name) {
    var attr = {
        type: "checkbox",
        name: name,
        id: name,
        value: isType(props.value, "string") ? props.value : ""
    };

    if ([undefined, false, 'false', 0, '0', null, "none", "no"].indexOf(props.checked) === -1) {
        attr['checked'] = "checked";
    }

    var label = isType(props.label, "string") ? props.label : "";
    var item = createItem('input', attr);

    return wrapItem(item, label, name);
}

function convertRadioField(props, labelKey, name) {
    if (!Array.isArray(props)) {
        throwError("Radio items should be as array.");
        return false;
    }
    var label = isType(labelKey, "string") ? labelKey : false;

    return wrapItem(props.map(function (item) {
        return createSwitcher(item, name, true);
    }), label, name);
}

function convertItem(item, index) {
    var name = 'name' + index;
    switch (item.type) {
        case "numberfield":
        case "number": {
            return convertNumberField(item, name);
        }
        case "textfield":
        case "text": {
            return convertTextField(item, name);
        }
        case "textarea": {
            return convertTextareaField(item, name);
        }
        case "checkbox": {
            return convertCheckboxField(item, name);
        }
        case "datefield":
        case "date": {
            return convertDateField(item, name);
        }
        case "radio buttons":
        case "radio":
        case "radiobuttons": {
            var list = item.items || item.list;
            if (!list) {
                throwError("Content radio should be array in field 'items' or 'list'");
                return false;
            }
            return convertRadioField(item.items || item.list, item.label, name);
        }
        default: {
            throwError("Type " + item.type + " is not accessible.");
            return false;
        }
    }
}

function convertItems(items) {
    if (!Array.isArray(items)) {
        throwError("Items is not array.");
        return "";
    }

    return items.map(function (item, index) {
        if (!isType(item, "object") || item === null) {
            return throwError("Item " + index + " is not a object.");
        }
        if (!isType(item.type, "string")) {
            return throwError("Item " + index + " don't have a type as string.")
        }
        return convertItem(item, index);
    }).filter(function (item) {
        return item;
    });
}

function checkTitle(title) {
    if (!isType(title, "string")) {
        throwError("Title is not a string");
        return "";
    }

    return title;
}

function convertJSON(json) {

    return (
        {
            title: checkTitle(json.title || ""),
            buttons: convertButtons(json.buttons || []),
            html: convertItems(json.items || [])
        }
    );
}

ready(function () {
    $('.apply').addEventListener('click', function () {
        try {
            var json = JSON5.parse($('.config > textarea').value);
        }
        catch (e) {
            alert('It is not a json.');
            return false;
        }
        try {
            var dataContainer = convertJSON(json);
        }
        catch (e) {
            alert(e);
            return false;
        }

        var buttonsBlock = $('.content > .result > .buttons');
        var contentBlock = $('.content > .result > .result-wrapper');
        buttonsBlock.innerHTML = "";
        contentBlock.innerHTML = "";

        $('.content > .result > .title').innerText = dataContainer.title;

        dataContainer.buttons.forEach(function (button) {
            buttonsBlock.append(button);
        });
        dataContainer.html.forEach(function (button) {
            contentBlock.append(button);
        });

        $('.tab.result').click();
    });
    $('.tab', true).forEach(function (tab) {
        tab.addEventListener('click', function (e) {
            var el = e.target;
            $('.tab', true).forEach(function (el) {
                removeClass(el, 'active');
            });
            $('.content > div', true).forEach(function (el) {
                removeClass(el, 'active');
            });

            var activeTypeClass = hasClass(el, 'config') ? '.config' : '.result';
            $(activeTypeClass, true).forEach(function (el) {
                addClass(el, 'active');
            });
        });
    });
});