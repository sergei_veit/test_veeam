function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

var $ = function (selector, isAll) {
    if (isAll) {
        return document.querySelectorAll(selector);
    }
    return document.querySelector(selector);
};

function hasClass(el, className) {
    if (el.classList) {
        return el.classList.contains(className);
    }
    return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}

function removeClass(el, className) {
    if (el.classList) {
        el.classList.remove(className);
        return;
    }
    el.className = el.className.replace(
        new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'),
        ' '
    );
}

function addClass(el, className) {
    if (el.classList) {
        el.classList.add(className);
        return;
    }
    el.className += ' ' + className;
}

function isType() {
    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments);
        var item = args.splice(0, 1)[0];
        return args.indexOf(typeof item) !== -1;
    }

    throwError("Too small arguments.");
    return false;
}


function throwError(message) {
    if (!IgnoreErrors) {
        alert(message);
        throw new Error(message);
    }

    console.log(message);
    return false;
}